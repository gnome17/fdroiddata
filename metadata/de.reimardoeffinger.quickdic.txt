Categories:Reading
License:Apache2
Web Site:
Source Code:https://github.com/rdoeffinger/Dictionary
Issue Tracker:https://github.com/rdoeffinger/Dictionary/issues
Changelog:https://github.com/rdoeffinger/Dictionary/releases

Auto Name:QuickDic
Summary:Offline translation dictionary
Description:
Resurrection of [[com.hughes.android.dictionary]] to a new maintainer. Uses data
from Wiktionary and Beolingus to generate dictionary files that can be used
offline. These can be downloaded from within the app. These can also be
generated automatically, though documentation for the process is currently
missing.
.

Repo Type:git
Repo:https://github.com/rdoeffinger/Dictionary.git

Build:5.1.3,41
    commit=v5.1.3
    gradle=yes
    srclibs=QuickdicUtilsRestored@d57bfc6bcf5b
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.1.4,42
    commit=v5.1.4
    gradle=yes
    srclibs=QuickdicUtilsRestored@d57bfc6bcf5b
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.1,52
    commit=v5.2.1
    gradle=yes
    srclibs=QuickdicUtilsRestored@9fbd31a944cf
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.2,53
    commit=v5.2.2
    gradle=yes
    srclibs=QuickdicUtilsRestored@9fbd31a944cf
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.3,54
    commit=v5.2.3
    gradle=yes
    srclibs=QuickdicUtilsRestored@9fbd31a944cf
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.4,57
    commit=v5.2.4
    gradle=yes
    srclibs=QuickdicUtilsRestored@9fbd31a944cf
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.6,63
    commit=v5.2.6
    gradle=yes
    srclibs=QuickdicUtilsRestored@ef7a142ae766f01
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.8,68
    commit=v5.2.8
    gradle=yes
    srclibs=QuickdicUtilsRestored@ef7a142ae766f01
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.9,69
    commit=v5.2.9
    gradle=yes
    srclibs=QuickdicUtilsRestored@ef7a142ae766f01
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.2.10,70
    commit=v5.2.10
    gradle=yes
    srclibs=QuickdicUtilsRestored@ef7a142ae766f01
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Build:5.3.0,77
    commit=v5.3.0
    gradle=yes
    srclibs=QuickdicUtilsRestored@ef7a142ae766f01
    prebuild=echo "quickdicUtilDir=$$QuickdicUtilsRestored$$" >> gradle.properties

Auto Update Mode:None
Update Check Mode:Tags ^v[0-9.]*$
Current Version:5.3.0
Current Version Code:77
