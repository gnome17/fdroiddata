Categories:Time
License:GPLv3
Web Site:http://arnef.github.io/bewegungsmelder-android
Source Code:https://github.com/arnef/bewegungsmelder-android
Issue Tracker:https://github.com/arnef/bewegungsmelder-android/issues

Auto Name:Bewegungsmelder
Summary:Get event information for Hamburg, Germany
Description:
Companion app for  [http://bewegungsmelder.nadir.org/ Bewegungsmelder], a
website focusing on converts and other events around Hamburg, Germany.
.

Repo Type:git
Repo:https://github.com/arnef/bewegungsmelder-android

Build:2.0.2,140629
    disable=res issues
    commit=9d21132dd0452cc155dfbe9fe7a25439293f7deb
    target=android-21

Maintainer Notes:
Specifies target=android-20 (Kitkat for Wachtes), bumping instead.
Doesn't compile, see https://github.com/arnef/bewegungsmelder-android/issues/7.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0.2
Current Version Code:140629
