Categories:Games
License:GPLv3
Web Site:
Source Code:https://github.com/peterosterlund2/droidfish
Issue Tracker:https://github.com/peterosterlund2/droidfish/issues

Auto Name:DroidFish
Summary:Chess program
Description:
Port of the CuckooChess applet/engine.

If you have [[org.scid.android]] installed you can read Scid database files
directly from the app by long-pressing the board and choosing Load game from
Scid file.
.

Repo Type:git
Repo:https://github.com/peterosterlund2/droidfish

Build:1.44,51
    commit=963
    subdir=DroidFish
    prebuild=cp -r ../CuckooChessEngine/src/* src/
    target=android-10
    buildjni=yes

Build:1.45,52
    commit=1044
    subdir=DroidFish
    prebuild=cp -r ../CuckooChessEngine/src/* src/ && \
        rm -rf obj/ assets/stockfish15.mygz && \
        $$NDK$$/ndk-build && \
        ant copy_stockfish -f build_copy_exe.xml
    target=android-10

Build:1.47,54
    commit=1080
    subdir=DroidFish
    prebuild=cp -r ../CuckooChessEngine/src/* src/ && \
        rm -rf obj/ assets/stockfish15.mygz && \
        $$NDK$$/ndk-build && \
        ant copy_stockfish -f build_copy_exe.xml
    target=android-10

Build:1.50,57
    commit=1113
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties && \
        rm assets/stockfish15.mygz
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.52,59
    commit=1124
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties && \
        rm assets/stockfish15.mygz
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.53 DD,60
    commit=1130
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties && \
        rm assets/stockfish15.mygz
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.54,61
    commit=1143
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.55,63
    commit=1188
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.56,64
    commit=1196
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.57,65
    commit=1202
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.61,69
    commit=e2538623d3aa92f3dca88519dbf8c9ca94e3b964
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties
    scanignore=CuckooChessEngine/src/book.bin
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.62,70
    commit=ad63e43412e631d78e3501b65990cddc1a350feb
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties
    scanignore=CuckooChessEngine/src/book.bin
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Build:1.63,71
    commit=5fd10a8e54bf5eda00c5a6db2ed26c9570ece076
    subdir=DroidFish
    prebuild=printf 'source.dir=src;../CuckooChessEngine/src' > ant.properties
    scanignore=CuckooChessEngine/src/book.bin
    build=$$NDK$$/ndk-build && \
        ant -f build_copy_exe.xml

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.63
Current Version Code:71
