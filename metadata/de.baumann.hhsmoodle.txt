Categories:Time,Science & Education
License:GPLv3+
Web Site:https://github.com/scoute-dich/HHSMoodle/blob/HEAD/README.md
Source Code:https://github.com/scoute-dich/HHSMoodle
Issue Tracker:https://github.com/scoute-dich/HHSMoodle/issues
Changelog:https://github.com/scoute-dich/HHSMoodle/blob/HEAD/CHANGELOG.md

Auto Name:HHS Moodle
Summary:Interact with HHS moodle instance
Description:
Interact with the Moodle instance of the [http://huebsch.karlsruhe.de/
Heinrich-Huebsch-Schule] in Karlsruhe, Germany.

[https://github.com/scoute-dich/HHSMoodle/blob/HEAD/SCREENSHOTS.md Screenshots]
.

Repo Type:git
Repo:https://github.com/scoute-dich/HHSMoodle

Build:1.0,2
    commit=v1.0
    subdir=app
    gradle=yes

Build:1.0.2,5
    commit=v1.0.2
    subdir=app
    gradle=yes

Build:1.0.3,8
    commit=v1.0.3
    subdir=app
    gradle=yes

Build:1.1,9
    commit=v1.1
    subdir=app
    gradle=yes

Build:2.0,10
    commit=v2.0
    subdir=app
    gradle=yes

Build:2.1,11
    commit=v2.1
    subdir=app
    gradle=yes

Build:2.2,14
    commit=v2.2
    subdir=app
    gradle=yes

Build:2.3.1,16
    commit=v2.3.1
    subdir=app
    gradle=yes

Build:2.3.3,18
    commit=v2.3.3
    subdir=app
    gradle=yes

Build:2.4.1,21
    commit=v2.4.1
    subdir=app
    gradle=yes

Build:2.4.2,22
    commit=v2.4.2
    subdir=app
    gradle=yes

Build:2.4.3,23
    commit=v2.4.3
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.4.3
Current Version Code:23
